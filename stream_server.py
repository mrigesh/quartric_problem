import socket
from socket import SOL_SOCKET, SO_REUSEADDR, AF_INET, SOCK_STREAM
import json
import pymongo
import cerberus
from settings import signal_schema
from helper import *

client = pymongo.MongoClient()
rules_db = client['quartic_rule_engine']['rules']

validator = cerberus.Validator(signal_schema)

port = 8001
s = socket.socket(AF_INET, SOCK_STREAM)
host = socket.gethostname()
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind((host, port))
s.listen(5)

print('Server listening....')
stream_data = ""
escape_character = "*"
data_set = {}
end_char = "}"
new_line_char = "\n"
current_char = ""
while True:
    data = "T"
    conn, addr = s.accept()
    while data:
        data = conn.recv(1)
        data = data.decode()
        if data == escape_character:
            conn.send(bytes(escape_character.encode()))
            break
        stream_data = stream_data + data
        if data == end_char:
            stream_data = json.loads(stream_data)
            signal = SignalData(stream_data)
            result = validator.validate(stream_data)
            if not result:
                message = "Validation failed with errors for signal " + str(signal.signal) + ". "
                errors = validator.errors
                for k, v in errors.items():
                    message = message + k + " " + v + ". "
                message = message + new_line_char
                conn.send(bytes(message.encode()))
                stream_data = ""
                continue
            rule_parser = RuleParser(rule_doc=rules_db, signal_data=signal)
            try:
                rule_parser.validate_signals()
                message = "Validation passed for signal " + signal.signal + new_line_char
                conn.send(bytes(message.encode()))
            except Exception as e:
                message = "Validation failed for signal " + signal.signal + " with errors. "+str(e)+ new_line_char
                conn.send(bytes(message.encode()))

            stream_data = ""
