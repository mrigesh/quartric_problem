from dateutil import parser


class SignalData():
    """
    SignalData class to take signal values and create an object for RuleParser class
    """
    def __init__(self, input_data):
        """

        Args:
            input_data: The signal data in dict format
        """
        self.signal = input_data.get('signal')
        self.value = input_data.get('value')
        self.value_type = input_data.get('value_type')
        self.input_data = input_data


class Rule():
    """
    Rule class to take rule values and create object for RuleParser class with respect to SignalData.signal value.
    """
    def __init__(self, rule_data):
        """

        Args:
            rule_data:
        """
        self.signal = rule_data.get('signal')
        self.condition = rule_data.get('condition')
        self.signal_type = rule_data.get('signal_type')
        self.signal_value = rule_data.get('signal_value')


class RuleParser():
    """
    RuleParser class to create objects of SignalData and Rule, proceeds with validations and returns elements passing the validations
    """
    def __init__(self, rule_doc, signal_data):
        """

        Args:
            rule_doc: mongodb instance for the rules document
            signal_data: signal data recieved from request object
        """
        self.document = rule_doc
        self.items = None
        self.signal_data = signal_data

    def validate_signals(self):
        """*
        Validates the signal data with respect to rules defined and returns dict of successful validations
        Returns:
            dict SignalData.input_data which has passed successful validations

        """
        signal = self.signal_data
        rule_data = self.document.find_one({"signal": signal.signal})
        try:
            rule = Rule(rule_data)
        except Exception as e:
            raise Exception("Rule with signal " + signal.signal + " not found.")
        try:
            if rule.signal_type != signal.value_type:
                raise TypeError(
                    signal.value_type + " does not conform to type required for signal " + rule.signal + " "
                                                                                                         "failing at required type being " + rule.signal_type)
            check = conditions[rule.signal_type][rule.condition](rule.signal_value, signal.value, rule.signal_type)
            if not check:
                raise ValueError(
                    signal.value + " validation failed for signal " + rule.signal + " with conditon " + rule.condition + " against rule value " + rule.signal_value)
        except Exception as e:
            raise Exception(e)
        return signal.input_data



def check_if_equal(rule_value, signal_value, condition_type=None):
    """

    Args:
        rule_value (str): value of rule for signal s
        signal_value (str): value of signal for signal s
        condition_type (str) : condition_type for the signal s as defined in rules

    Returns:
        bool: True if validation pass

    Raises:
        TypeError: on type mismatch
        ValueError: on condition mismatch

    """
    if not isinstance(signal_value, str):
        raise TypeError(str(signal_value) + " is not of type" + condition_type)
    if signal_value != rule_value:
        raise ValueError(
            signal_value + " does not match the rule for type " + condition_type + " with value " + rule_value)
    return True


def check_if_lesser(rule_value, signal_value, condition_type=None):
    """

    Args:
        rule_value (str): value of rule for signal s
        signal_value (str): value of signal for signal s
        condition_type (str) : condition_type for the signal s as defined in rules

    Returns:
        bool: True if success else False

    Raises:
        ValueError: on invalid data with respect to condition(less_than, greater_than)

    """
    if condition_type == 'Integer':
        rule_value = int(rule_value)
        try:
            signal_value = int(signal_value)
            if signal_value > rule_value:
                return False
        except ValueError as e:
            raise ValueError(signal_value + " invalid data for type " + condition_type)
    elif condition_type == 'Datetime':
        rule_value = parser.parse(rule_value)
        try:
            signal_value = parser.parse(signal_value)
            if signal_value > rule_value:
                return False
        except ValueError as e:
            raise ValueError(str(signal_value) + " invalid data for type " + condition_type)
    return True


def check_if_greater(rule_value, signal_value, condition_type=None):
    """

    Args:
        rule_value (str): value of rule for signal s
        signal_value (str): value of signal for signal s
        condition_type (str) : condition_type for the signal s as defined in rules

    Returns:
        bool: True if success else False

    Raises:
        ValueError: on invalid data with respect to condition(less_than, greater_than)

    """
    if condition_type == 'Integer':
        rule_value = int(rule_value)
        try:
            signal_value = int(signal_value)
            if signal_value < rule_value:
                return False
        except ValueError as e:
            raise ValueError(signal_value + " invalid data for type " + condition_type)
    elif condition_type == 'Datetime':
        rule_value = parser.parse(rule_value)
        try:
            signal_value = parser.parse(signal_value)
            if signal_value < rule_value:
                return False
        except ValueError as e:
            raise ValueError(str(signal_value) + " invalid data for type " + condition_type)
    return True


conditions = {
    'String': {'equals': check_if_equal},
    'Integer': {
        'equals': check_if_equal,
        'less_than': check_if_lesser,
        'greater_than': check_if_greater,
    },
    'Datetime': {
        'equals': check_if_equal,
        'less_than': check_if_lesser,
        'greater_than': check_if_greater,
    }
}
