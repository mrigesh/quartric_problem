PIP = $(shell which pip)
GIT = $(shell which git)
PYTEST = $(shell which pytest)
PYTHON = $(shell which python)
FLASK = $(shell which flask)
AB = $(shell which ab)


prepare:
	$(PIP) install -r requirements.txt

test_code:
	$(PYTEST) test_quartic.py

test_api:
	$(PYTHON) test_quartic_api.py

run_server:
	$(PYTHON) quartic.py

benchmark:
	$(AB) -n 100 -c 100 -k -T application/json -p test_data.txt http://127.0.0.1:5000/v1/signal


