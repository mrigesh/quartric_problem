from unittest import TestCase
from .helper import Rule, SignalData, RuleParser, check_if_equal, check_if_greater, check_if_lesser

SAMPLE_SIGNAL_DATA = [{"signal": "ATL2", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL9", "value_type": "Datetime", "value": "2017-06-13 22:40:10"},
                      {"signal": "ATL3", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL8", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL1", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL1", "value_type": "Datetime", "value": "2017-04-10 10:16:55"},
                      {"signal": "ATL6", "value_type": "Integer", "value": "75.361"},
                      {"signal": "ATL3", "value_type": "Datetime", "value": "2017-04-21 07:55:28"},
                      {"signal": "ATL2", "value_type": "Datetime", "value": "2017-05-13 06:22:35"},
                      {"signal": "ATL10", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL10", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL6", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL10", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL5", "value_type": "Datetime", "value": "2017-07-21 20:20:21"},
                      {"signal": "ATL10", "value_type": "Datetime", "value": "2017-07-26 16:35:11"},
                      {"signal": "ATL2", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL8", "value_type": "Datetime", "value": "2017-05-18 03:51:34"},
                      {"signal": "ATL3", "value_type": "Integer", "value": "65.236"},
                      {"signal": "ATL1", "value_type": "Datetime", "value": "2017-02-18 16:49:03"},
                      {"signal": "ATL4", "value_type": "Integer", "value": "5.128"},
                      {"signal": "ATL10", "value_type": "Integer", "value": "46.691"},
                      {"signal": "ATL8", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL1", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL5", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL10", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL3", "value_type": "Datetime", "value": "2017-07-14 19:13:37"},
                      {"signal": "ATL7", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL10", "value_type": "Datetime", "value": "2017-04-21 16:22:11"},
                      {"signal": "ATL8", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL9", "value_type": "Datetime", "value": "2017-01-10 01:13:47"},
                      {"signal": "ATL9", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL5", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL7", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL5", "value_type": "Integer", "value": "48.987"},
                      {"signal": "ATL5", "value_type": "Datetime", "value": "2017-06-20 10:53:20"},
                      {"signal": "ATL2", "value_type": "Integer", "value": "54.026"},
                      {"signal": "ATL3", "value_type": "Datetime", "value": "2017-10-06 04:19:46"},
                      {"signal": "ATL1", "value_type": "Integer", "value": "12.515"},
                      {"signal": "ATL5", "value_type": "Integer", "value": "34.458"},
                      {"signal": "ATL10", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL9", "value_type": "Integer", "value": "5.534"},
                      {"signal": "ATL9", "value_type": "Integer", "value": "79.476"},
                      {"signal": "ATL2", "value_type": "Integer", "value": "84.813"},
                      {"signal": "ATL9", "value_type": "Datetime", "value": "2017-08-09 15:44:47"},
                      {"signal": "ATL2", "value_type": "Datetime", "value": "2017-08-20 07:25:29"},
                      {"signal": "ATL10", "value_type": "String", "value": "LOW"},
                      {"signal": "ATL10", "value_type": "Datetime", "value": "2017-08-02 05:20:21"},
                      {"signal": "ATL4", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL5", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL9", "value_type": "Datetime", "value": "2017-07-15 02:40:17"},
                      {"signal": "ATL9", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL5", "value_type": "String", "value": "HIGH"},
                      {"signal": "ATL1", "value_type": "Integer", "value": "63.679"}]
SAMPLE_RULE_DATA = [
    {"signal": "test1", "signal_value": "2017/03/07", "signal_type": "Datetime", "condition": "less_than"}]

SAMPLE_CHECK_EQUAL_DATA = {
    'positive': [{'rule_value': 'a', 'signal_value': 'a', 'condition_type': 'String'}],
    'negative': [{'rule_value': 'a', 'signal_value': 'b', 'condition_type': 'String'}],
    'invalid': [{'rule_value': 'a', 'signal_value': 1, 'condition_type': 'String'}]
}

SAMPLE_CHECK_LESSER_DATA = {
    'positive': [{'rule_value': '3', 'signal_value': '2', 'condition_type': 'Integer'}, {'rule_value': '2018/01/01',
                                                                                         'signal_value': '2017/01/01',
                                                                                         'condition_type': 'Datetime'}],
    'negative': [{'rule_value': '1', 'signal_value': '2', 'condition_type': 'Integer'}, {'rule_value': '2017/01/01',
                                                                                         'signal_value': '2018/01/01',
                                                                                         'condition_type': 'Datetime'}],
    'invalid': [{'rule_value': '1', 'signal_value': 'a', 'condition_type': 'Integer'}, {'rule_value': '2017/01/01',
                                                                                        'signal_value': '2018/01/a1',
                                                                                        'condition_type': 'Datetime'}],
}
SAMPLE_CHECK_GREATER_DATA = {
    'positive': [{'rule_value': '1', 'signal_value': '2', 'condition_type': 'Integer'}, {'rule_value': '2017/01/01',
                                                                                         'signal_value': '2018/01/01',
                                                                                         'condition_type': 'Datetime'}],
    'negative': [{'rule_value': '3', 'signal_value': '2', 'condition_type': 'Integer'}, {'rule_value': '2018/01/01',
                                                                                         'signal_value': '2017/01/01',
                                                                                         'condition_type': 'Datetime'}],
    'invalid': [{'rule_value': '1', 'signal_value': 'a', 'condition_type': 'Integer'}, {'rule_value': '2017/01/01',
                                                                                        'signal_value': '2018/01/a1',
                                                                                        'condition_type': 'Datetime'}],
}


class TestSignalData(TestCase):
    def test_signal_data(self):
        for sample_data in SAMPLE_SIGNAL_DATA:
            signal_data = SignalData(sample_data)
            signal = sample_data.get('signal')
            value_type = sample_data.get('value_type')
            value = sample_data.get('value')
            input_data = sample_data
            self.assertIsInstance(signal_data, SignalData)
            self.assertEqual(signal_data.signal, signal)
            self.assertEqual(signal_data.input_data, input_data)
            self.assertEqual(signal_data.value_type, value_type)
            self.assertEqual(signal_data.value, value)


class TestRule(TestCase):
    def test_rule_data(self):
        for sample_rule in SAMPLE_RULE_DATA:
            rule = Rule(sample_rule)
            signal = sample_rule.get('signal')
            signal_value = sample_rule.get('signal_value')
            signal_type = sample_rule.get('signal_type')
            condition = sample_rule.get('condition')
            self.assertIsInstance(rule, Rule)
            self.assertEqual(signal, rule.signal)
            self.assertEqual(signal_value, rule.signal_value)
            self.assertEqual(signal_type, rule.signal_type)
            self.assertEqual(condition, rule.condition)


# class TestRuleParser(TestCase):
#     def test_validate_signals(self):
#         client = pymongo.MongoClient()
#         rules_doc = client.quartric_rule_engine.rules
#         for sample_data in SAMPLE_SIGNAL_DATA:
#             signal_data = SignalData(sample_data)
#             rule_parser = RuleParser(rules_doc, signal_data)
#             rule_parser.validate_signals()


class TestCheck_if_equal(TestCase):
    def test_check_if_equal_positive(self):
        positive_values = SAMPLE_CHECK_EQUAL_DATA.get('positive')
        for values in positive_values:
            self.assertTrue(check_if_equal(values.get('rule_value'), values.get('signal_value'), values.get(
                'condition_type')))

    def test_check_if_equal_negative(self):
        negative_values = SAMPLE_CHECK_EQUAL_DATA.get('negative')
        for values in negative_values:
            self.assertRaises(ValueError, check_if_equal, values.get('rule_value'), values.get('signal_value'),
                              values.get('condition_type'))

    def test_check_if_equal_invalid(self):
        invalid_values = SAMPLE_CHECK_EQUAL_DATA.get('invalid')
        for values in invalid_values:
            self.assertRaises(TypeError, check_if_equal, values.get('rule_value'), values.get('signal_value'),
                              values.get('condition_type'))


class TestCheck_if_greater(TestCase):
    def test_check_if_greater_positive(self):
        positive_values = SAMPLE_CHECK_GREATER_DATA.get('positive')
        for values in positive_values:
            self.assertTrue(check_if_greater(values.get('rule_value'), values.get('signal_value'), values.get(
                'condition_type')))

    def test_check_if_greater_negative(self):
        negative_values = SAMPLE_CHECK_GREATER_DATA.get('negative')
        for values in negative_values:
            self.assertFalse(check_if_greater(values.get('rule_value'), values.get('signal_value'), values.get(
                'condition_type')))

    def test_check_if_greater_invalid(self):
        invalid_values = SAMPLE_CHECK_GREATER_DATA.get('invalid')
        for values in invalid_values:
            self.assertRaises(ValueError, check_if_greater, values.get('rule_value'),values.get('signal_value'),
                              values.get('condition_type'))


class TestCheck_if_lesser(TestCase):
    def test_check_if_lesser_positive(self):
        positive_values = SAMPLE_CHECK_LESSER_DATA.get('positive')
        for values in positive_values:
            self.assertTrue(check_if_lesser(values.get('rule_value'), values.get('signal_value'), values.get(
                'condition_type')))

    def test_check_if_lesser_negative(self):
        negative_values = SAMPLE_CHECK_LESSER_DATA.get('negative')
        for values in negative_values:
            self.assertFalse(check_if_lesser(values.get('rule_value'), values.get('signal_value'), values.get(
                'condition_type')))

    def test_check_if_lesser_invalid(self):
        invalid_values = SAMPLE_CHECK_LESSER_DATA.get('invalid')
        for values in invalid_values:
            self.assertRaises(ValueError, check_if_lesser, values.get('rule_value'),values.get('signal_value'),
                              values.get('condition_type'))
