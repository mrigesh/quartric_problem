import logging
from eve import Eve
from flask import abort
from helper import *

RULES = {}

app = Eve()

def signal_validation(request):
    """
    This function loads the json data from request object, processes the same with validations with regards to rules
    and proceeds with only those data elements which have passed the validations of rule engine

    Args:
        request: Python http request object provided by on_pre_POST_<resource_name>
    """
    items = request.json
    rules_doc = app.data.driver.db['rules']
    validated_items = []
    for i in items:
        signal_data = SignalData(i)
        rule_parser = RuleParser(rules_doc, signal_data)
        try:
            validated_items.append(rule_parser.validate_signals())
            app.logger.info("Rule validation passed for signal "+ signal_data.signal)
        except Exception as e:
            app.logger.info(e)
    if not validated_items:
        abort(400)
    request.json.clear()
    request.json.extend(validated_items)

app.on_pre_POST_signal += signal_validation

if __name__ == '__main__':
    handler = logging.FileHandler('app.log')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(filename)s:%(lineno)d] -- ip: %(clientip)s, '
        'url: %(url)s, method:%(method)s'))
    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run()

