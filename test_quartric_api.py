import json
import os
import tempfile
import time
import unittest
from http import HTTPStatus

import quartic


class QuarticTestCase(unittest.TestCase):
    rules_duplicate = []

    def setUp(self):
        self.db_fd, quartic.app.config['DATABASE'] = tempfile.mkstemp()
        quartic.app.config['TESTING'] = True
        quartic.app.config["MONGO_DBNAME"] = 'quartic_rule_engine_test'
        self.app = quartic.app.test_client()

    def tearDown(self):
        time.sleep(1)
        os.close(self.db_fd)
        os.unlink(quartic.app.config['DATABASE'])

    def clear_db(self):
        with quartic.app.app_context():
            rules = quartic.app.data.driver.db['rules']
            signal = quartic.app.data.driver.db['signal']
            rules.drop()
            signal.drop()

    def test_root(self):
        rv = self.app.get('/')
        data = json.loads(rv.data)
        code = data.get('error').get('code')
        self.assertEqual(int(code), HTTPStatus.NOT_FOUND)

    def test_api_v1(self):
        title_list = ['rules', 'signal', 'schema']
        rv = self.app.get('/v1/')
        data = json.loads(rv.data)
        resources = data.get('_links').get('child')
        for r in resources:
            self.assertIn(r.get('title'), title_list)

    def test_api_schema(self):
        rv = self.app.get('/v1/schema')
        status_code = rv._status_code
        self.assertEqual(status_code, HTTPStatus.OK)

    def test_api_signal(self):
        rv = self.app.get('/v1/signal')
        status_code = rv._status_code
        self.assertEqual(status_code, HTTPStatus.OK)

    def test_api_rules(self):
        rv = self.app.get('/v1/rules')
        status_code = rv._status_code
        self.assertEqual(status_code, HTTPStatus.OK)

    def test_api_rules_create_success(self):
        self.clear_db()
        for i in range(1000, 1100, 3):
            data = [{"signal": i, "signal_value": "2017/03/07", "signal_type": "Datetime", "condition": "less_than"},
                    {"signal": i + 1, "signal_value": "HIGH", "signal_type": "String", "condition": "equals"},
                    {"signal": i + 2, "signal_value": "1", "signal_type": "Integer", "condition": "greater_than"}]
            for d in data:
                rv = self.app.post('/v1/rules', data=d)
                status_code = rv._status_code
                self.assertEqual(status_code, HTTPStatus.CREATED)

    def test_api_rules_update_success(self):
        for i in range(1000, 1100, 3):
            data = [{"signal": str(i), "signal_value": "2017/03/08", "signal_type": "Datetime", "condition": \
                "less_than"},
                    {"signal": str(i + 1), "signal_value": "LOW", "signal_type": "String", "condition": "equals"},
                    {"signal": str(i + 2), "signal_value": "2", "signal_type": "Integer", "condition": "greater_than"}]
            for d in data:
                rv = self.app.patch('/v1/rules/' + d.get('signal'), data={'signal_value': d.get('signal_value')})
                status_code = rv._status_code
                self.assertEqual(status_code, HTTPStatus.OK)

    def test_api_rules_create_failure(self):
        for i in range(1000, 1100, 3):
            data = [{"signal": i, "signal_value": "2017/03/07", "signal_type": "Datetime", "condition": "less_than"},
                    {"signal": i + 1, "signal_value": "HIGH", "signal_type": "String", "condition": "equals"},
                    {"signal": i + 2, "signal_value": "1", "signal_type": "Integer", "condition": "greater_than"}]
            for d in data:
                rv = self.app.post('/v1/rules', data=d)
                status_code = rv._status_code
                self.assertEqual(status_code, HTTPStatus.UNPROCESSABLE_ENTITY)


if __name__ == '__main__':
    unittest.main()
