MONGO_DBNAME = 'quartic_rule_engine'
API_VERSION = 'v1'
ERROR = 'error'
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PUT', 'PATCH', 'DELETE']
CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20
IF_MATCH = False
SCHEMA_ENDPOINT = 'schema'
DEBUG=False

rules_schema = {
    'signal': {
        'type': 'string',
        'minlength': 4,
        'maxlength': 10,
        'required': True,
        'unique': True,
        'empty': False,
    },
    'condition': {
        'type': 'string',
        'allowed': ['equals', 'greater_than', 'less_than'],
        'required': True,
    },
    'signal_type':{
        'type':'string',
        'allowed':['Integer','String','Datetime'],
        'required': True
    },
    'signal_value':{
        'type':'string',
        'minlength':1,
        'required':True,
        'empty':False

    }
}
rules = {
    'item_title': 'rules',
    'schema': rules_schema,
    'id_field': 'signal',
    'item_lookup_field': 'signal',
    'item_url': r'regex("[\w]+")',
    'item_methods': ['GET', 'PUT', 'PATCH', 'DELETE'],
}

signal_schema = {
    'signal': {
        'type': 'string',
        'minlength': 4,
        'maxlength': 10,
        'required': True,
        'empty': False,
    },
    'value': {
        'type': 'string',
        'minlength': 1,
        'required': True,
        'empty': False
    },
    'value_type': {
        'type': 'string',
        'allowed': ['Integer', 'String', 'Datetime'],
        'required': True,
        'empty': False,
    }
}
signals = {
    'item_title': 'signal',
    'schema': signal_schema,
    'id_field': 'signal',
    'item_methods': ['GET'],
}
DOMAIN = {
    'rules': rules,
    'signal':signals
}
